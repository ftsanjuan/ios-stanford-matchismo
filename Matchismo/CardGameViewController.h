//
//  CardGameViewController.h
//  Matchismo
//
//  Created by Francis San Juan on 2013-07-15.
//  Copyright (c) 2013 Francis San Juan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Deck.h"

// An abstract ViewController class for a generic card game
@interface CardGameViewController : UIViewController

@property (nonatomic) NSUInteger startingCardCount; // abstract!
- (void)updateCell:(UICollectionViewCell *)cell usingCard:(Card *)card; // abstract!
- (Deck *)createDeck; // abstract!

@end
