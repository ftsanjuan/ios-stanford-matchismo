//
//  PlayingCardGameViewController.h
//  Matchismo
//
//  Created by Francis San Juan on 2013-08-07.
//  Copyright (c) 2013 Francis San Juan. All rights reserved.
//

#import "CardGameViewController.h"

@interface PlayingCardGameViewController : CardGameViewController

@end
